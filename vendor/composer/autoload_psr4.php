<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Picqer\\Barcode\\' => array($vendorDir . '/picqer/php-barcode-generator/src'),
    'Packages\\' => array($baseDir . '/packages'),
    'Core\\' => array($baseDir . '/core'),
    'Config\\' => array($baseDir . '/configs'),
    'App\\' => array($baseDir . '/app'),
);
